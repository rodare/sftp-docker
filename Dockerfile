FROM atmoz/sftp:alpine

COPY sshd_config /etc/ssh/sshd_config
COPY entrypoint.sh /

EXPOSE 22

ENTRYPOINT ["/entrypoint.sh"]